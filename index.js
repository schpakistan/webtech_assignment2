
const FIRST_NAME = "Ana-Maria";
const LAST_NAME = "Schpak";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
function initCaching() {
var empty = {};
empty.about=0;
empty.contact=0;
empty.home=0;

   var obj = Object.create( {
       pageAccessCounter: function(accessedSection){
            if('about'.localeCompare(accessedSection, 'en', {sensitivity: 'accent'}) == 0){
                empty.about++;
            }
            if(accessedSection === 'contact'){
                empty.contact++;
            }
            if(!(accessedSection)){
                empty.home++;
            }
       },

       getCache: function(){
            return empty;
       }, 
   }); 
   
   return obj;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

